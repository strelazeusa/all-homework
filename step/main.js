//* functions section Our Services

let itemServices = document.querySelectorAll(".services_item");
let content = document.querySelectorAll(".discription_services");

itemServices.forEach((item) =>
  item.addEventListener("click", function (event) {
    let activeTabs = document.querySelector(".active");
    let activeTab = event.target.getAttribute("data-tab-item");

    console.log(activeTabs);
    for (let elem of content) {
      if (activeTab === elem.getAttribute("data-content")) {
        elem.style.display = "flex";
      } else {
        elem.style.display = "none";
      }
    }
    if (item !== activeTabs) {
      activeTabs.classList.remove("active");
      item.classList.add("active");
    }
  })
);

//*  functions section Our Amazing Work

const allImg = [
  "./img/work/graphic_design/Graphic Design.png",
  "./img/work/graphic_design/graphic_design2.png",
  "./img/work/graphic_design/graphic_design3.png",
  "./img/work/graphic_design/graphic_design4.webp",
  "./img/work/graphic_design/graphic_design5.jpg",
  "./img/work/graphic_design/graphic_design6.png",
  "./img/work/graphic_design/graphic_design7.jpg",
  "./img/work/graphic_design/graphic_design8.jpg",
  "./img/work/graphic_design/graphic_design9.jpeg",
  "./img/work/graphic_design/graphic_design10.jpeg",
  "./img/work/graphic_design/graphic_design11.jpg",
  "./img/work/graphic_design/graphic_design12.jpg",
  "./img/work/web_design/web-design_1.jpg",
  "./img/work/web_design/web-design_2.jpg",
  "./img/work/web_design/web-design_3.jpeg",
  "./img/work/web_design/web-design_4.png",
  "./img/work/web_design/web-design_5.jpeg",
  "./img/work/web_design/web-design_6.jpg",
  "./img/work/web_design/web-design_7.jpg",
  "./img/work/web_design/web-design_8.jpg",
  "./img/work/web_design/web-design_9.jpeg",
  "./img/work/web_design/web-design_10.png",
  "./img/work/web_design/web-design_11.jpg",
  "./img/work/web_design/web-design_12.jpg",
  "./img/work/landing_pages/landing_page_1.png",
  "./img/work/landing_pages/landing_page_2.png",
  "./img/work/landing_pages/landing_page_3.png",
  "./img/work/landing_pages/landing_page_4.jpeg",
  "./img/work/landing_pages/landing_page_5.png",
  "./img/work/landing_pages/landing_page_6.jpeg",
  "./img/work/landing_pages/landing_page_7.jpeg",
  "./img/work/landing_pages/landing_page_8.png",
  "./img/work/landing_pages/landing_page_9.png",
  "./img/work/landing_pages/landing_page_10.jpg",
  "./img/work/landing_pages/landing_page_11.png",
  "./img/work/landing_pages/landing_page_12.png",
  "./img/work/wordpress/wordpress_1.png",
  "./img/work/wordpress/wordpress_2.jpg",
  "./img/work/wordpress/wordpress_3.jpg",
  "./img/work/wordpress/wordpress_4.jpg",
  "./img/work/wordpress/wordpress_5.png",
  "./img/work/wordpress/wordpress_6.jpg",
  "./img/work/wordpress/wordpress_7.jpg",
  "./img/work/wordpress/wordpress_8.jpg",
  "./img/work/wordpress/wordpress_9.jpg",
  "./img/work/wordpress/wordpress_10.png",
  "./img/work/wordpress/wordpress_11.jpg",
  "./img/work/wordpress/wordpress_12.png",
];

const graphicDesign = [
  "./img/work/graphic_design/Graphic Design.png",
  "./img/work/graphic_design/graphic_design2.png",
  "./img/work/graphic_design/graphic_design3.png",
  "./img/work/graphic_design/graphic_design4.webp",
  "./img/work/graphic_design/graphic_design5.jpg",
  "./img/work/graphic_design/graphic_design6.png",
  "./img/work/graphic_design/graphic_design7.jpg",
  "./img/work/graphic_design/graphic_design8.jpg",
  "./img/work/graphic_design/graphic_design9.jpeg",
  "./img/work/graphic_design/graphic_design10.jpeg",
  "./img/work/graphic_design/graphic_design11.jpg",
  "./img/work/graphic_design/graphic_design12.jpg",
];

const button = document.querySelector(".btn_work");
const btnImg = document.querySelector(".btn_work img");
const buttonImg = document.querySelectorAll(".lds-ring div");
const gridItemsWork = document.querySelector(".grid_items_work");
const tabWork = document.querySelector(".tab_work");

//function of simulating loading from the server
button.addEventListener("click", function (e) {
  e.preventDefault();

  for (let item of buttonImg) {
    item.style.display = "block";
    btnImg.style.display = "none";
    setTimeout(function () {
      item.style.display = "none";
      btnImg.style.display = "block";
    }, 1000);
  }
  setTimeout(function () {
    // function for capturing through 12 pictures
    renderAllImg(allImg);
    if (allImg.length == 0) {
      button.style.display = "none";
    }
  }, 1000);
});

// * function switch tabs section Our Amazing Work

function clearWorkTabsGrid() {
  gridItemsWork.innerHTML = "";
}

tabWork.addEventListener("click", () => {
  let target = event.target.textContent;

  if (target == "All") {
    clearWorkTabsGrid();
    switchWorkTab(allImg);
  } else if (target == "Graphic Design") {
    clearWorkTabsGrid();
    const getGraphicDesign = (from = 0, limit = 12) => {
      return allImg.slice(0, 12);
    };
    switchWorkTab(getGraphicDesign(allImg));
  } else if (target == "Web Design") {
    clearWorkTabsGrid();
    const getWebDesign = (from = 13, limit = 25) => {
      return allImg.slice(12, 24);
    };
    switchWorkTab(getWebDesign(allImg));
  } else if (target == "Landing Pages") {
    clearWorkTabsGrid();
    const getLandingPages = (from = 26, limit = 36) => {
      return allImg.slice(24, 36);
    };
    switchWorkTab(getLandingPages(allImg));
  } else if (target == "Wordpress") {
    clearWorkTabsGrid();
    const getWordpress = (from = 36, limit = 48) => {
      return allImg.slice(36, 48);
    };
    switchWorkTab(getWordpress(allImg));
  }
});
function switchWorkTab(allImg) {
  for (let imgSrc of allImg) {
    const div = document.createElement("div");
    div.style.display = "block";
    gridItemsWork.style.marginBottom = "50px";

    div.classList.add("item_work_img");

    const img = document.createElement("img");
    img.src = imgSrc;
    img.alt = "";

    const imgHover = document.createElement("div");
    imgHover.classList.add("img_hover_work");

    const imgHoverContainer = document.createElement("div");
    imgHoverContainer.classList.add("img_hover_container");

    const hoverImg = document.createElement("img");
    hoverImg.src = "./img/work/hover_img/icon.png";
    hoverImg.alt = "hove_img";

    const titleContainer = document.createElement("div");
    titleContainer.classList.add("title_item_work_container");

    const titleLink = document.createElement("a");
    titleLink.href = "#";
    titleLink.classList.add("title_item_work");
    titleLink.textContent = "creative design";

    const descriptionContainer = document.createElement("div");
    descriptionContainer.classList.add("discreption_item_work_container");

    const descriptionLink = document.createElement("a");
    descriptionLink.href = "#";
    descriptionLink.classList.add("discription_item_work");
    descriptionLink.textContent = "Graphic Design";

    gridItemsWork.append(div);
    div.append(img);
    div.append(imgHover);
    imgHover.append(imgHoverContainer);
    imgHoverContainer.append(hoverImg);
    imgHover.append(titleContainer);
    titleContainer.append(titleLink);
    imgHover.append(descriptionContainer);
    descriptionContainer.append(descriptionLink);
  }
}

function renderAllImg(allImg) {
  const getImgs = (from = 0, limit = 12) => {
    return allImg.splice(from, limit);
  };

  for (let imgSrc of getImgs()) {
    const div = document.createElement("div");
    div.style.display = "block";
    gridItemsWork.style.marginBottom = "50px";

    div.classList.add("item_work_img");

    const img = document.createElement("img");
    img.src = imgSrc;
    img.alt = "";

    const imgHover = document.createElement("div");
    imgHover.classList.add("img_hover_work");

    const imgHoverContainer = document.createElement("div");
    imgHoverContainer.classList.add("img_hover_container");

    const hoverImg = document.createElement("img");
    hoverImg.src = "./img/work/hover_img/icon.png";
    hoverImg.alt = "hove_img";

    const titleContainer = document.createElement("div");
    titleContainer.classList.add("title_item_work_container");

    const titleLink = document.createElement("a");
    titleLink.href = "#";
    titleLink.classList.add("title_item_work");
    titleLink.textContent = "creative design";

    const descriptionContainer = document.createElement("div");
    descriptionContainer.classList.add("discreption_item_work_container");

    const descriptionLink = document.createElement("a");
    descriptionLink.href = "#";
    descriptionLink.classList.add("discription_item_work");
    descriptionLink.textContent = "Graphic Design";

    gridItemsWork.append(div);
    div.append(img);
    div.append(imgHover);
    imgHover.append(imgHoverContainer);
    imgHoverContainer.append(hoverImg);
    imgHover.append(titleContainer);
    titleContainer.append(titleLink);
    imgHover.append(descriptionContainer);
    descriptionContainer.append(descriptionLink);
  }
}

// * Section Client Feedback

const imgAvatarClient = [
  "./img/client_feedback/avatar_1.png",
  "./img/client_feedback/avatar_2.png",
  "./img/client_feedback/avatar_3.png",
  "./img/client_feedback/avatar_4.png",
];

let avatarIteme = document.querySelectorAll(".avatar_iteme");
let nextClient = document.querySelector(".next_client");
let photoItem = document.querySelectorAll(".photo_item");
let leftBtn = document.querySelector(".next_btn_left");
let rightBtn = document.querySelector(".next_btn_right");
let nameClient = document.querySelectorAll(".name_client");
let discriptionWrapper = document.querySelectorAll(".discription_wrapper");


function removeActivTab() {
  for (let elem of avatarIteme) {
    elem.classList.remove("active_avatar");
  }
  for (let name of nameClient) {
    name.classList.remove("active-name");
  }
  for (let wrapper of discriptionWrapper) {
    wrapper.classList.remove("active_wrapper");
  }
  for (let img of photoItem) {
    img.classList.remove("active_img");
  }
}

// function chois avatar section Avatar client
function choisAvatar() {
  for (let item of avatarIteme) {
    item.addEventListener("click", () => {
      removeActivTab();
      item.classList.add("active_avatar");
      let target = event.target.dataset.name;
      // let activeAvtar = event.target.closest("div");
      // // console.log(activeAvtar)

      for (let name of nameClient) {
        if (target === name.textContent) {
          name.classList.add("active-name");
        }
      }

      for (let wrapper of discriptionWrapper) {
        if (target === wrapper.getAttribute("data-wrapper")) {
          wrapper.classList.add("active_wrapper");
        }
      }

      for (let img of photoItem) {
        if (target === img.getAttribute("data-img")) {
          img.classList.add("active_img");
        }
      }
    });
  }
}

choisAvatar();

// function arrows btn left and right
function handleImgeChange(offset) {
  const activeSlide = document.querySelector(".active_avatar");
  let slides = [...document.querySelectorAll(".avatar_iteme")];
  const currentIndex = slides.indexOf(activeSlide);

  let newIndex = currentIndex + offset;
  if (newIndex < 0) newIndex = slides.length - 1;
  if (newIndex >= slides.length) newIndex = 0;

  slides[newIndex].classList.add("active_avatar");
  activeSlide.classList.remove("active_avatar");

  console.log(slides[newIndex].getAttribute("data-name"));
  let activeArrow = slides[newIndex].getAttribute("data-name");

  for (let name of nameClient) {
    if (activeArrow === name.textContent) {
      name.classList.add("active-name");
    } else {
      name.classList.remove("active-name");
    }
  }
  for (let wrapper of discriptionWrapper) {
    if (activeArrow === wrapper.getAttribute("data-wrapper")) {
      wrapper.classList.add("active_wrapper");
    } else {
      wrapper.classList.remove("active_wrapper");
    }
  }

  for (let img of photoItem) {
    if (activeArrow === img.getAttribute("data-img")) {
      img.classList.add("active_img");
    } else {
      img.classList.remove("active_img");
    }
  }



}

leftBtn.addEventListener("click", () => {
  handleImgeChange(-1);
});

rightBtn.addEventListener("click", () => {
  handleImgeChange(1);
  choisAvatar();
});
