let passwordForm = document.querySelector(".password-form");
let jsFirstPassword = document.querySelector(".js-first-password");
let firstIconPassword = document.querySelector("#first-icon-password");
let jsSecondPassword = document.querySelector(".js-second-password");
let secondIconPassword = document.getElementById("second-icon-password");
let btn = document.querySelector(".btn");

function showPassword(input, icon) {
  passwordForm.addEventListener("click", function () {
    let target = event.target.id;

    if (target == icon.id) {
      if (input.getAttribute("type") === "password") {
        input.setAttribute("type", "text");
        icon.className = "fas fa-eye-slash icon-password";
      } else {
        input.setAttribute("type", "password");
        icon.className = "fas fa-eye icon-password";
      }
    }
  });

  function btnForm(inputFormOne, inputFormTwo) {
    passwordForm.onsubmit = function (event) {
      if (inputFormOne.value !== inputFormTwo.value) {
        event.preventDefault();

        btn.insertAdjacentHTML(
          "beforebegin",
          `<p class="p">Потрібно ввести однакові значення</p>`
        );
        console.log("не правильный пароль");
      } else {
        alert("You are welcome");
      }
    };
  }

  btnForm(jsFirstPassword, jsSecondPassword);
}
showPassword(jsFirstPassword, firstIconPassword);
showPassword(jsSecondPassword, secondIconPassword);
