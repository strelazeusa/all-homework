## Опишіть своїми словами як працює метод forEach.

Це метод який викликає колбек функцію для кожного елемента масива, і перебирає його повертаючи ті елементи які відповідають умові. Можна використовувати три параметри(елемент, індекс, масив). Нічого не повертає окрім елементів масиву.

## Як очистити масив?

Можна використати декілька методів в залежності від задачі. Самий наче правильний метод це через arr.splice(передає індекс з якого по яке вирізати елементи). В такому варіанті у масиві не залишиться пустих місць, що дозволить коректно потім працювати з масивом.

## Як можна перевірити, що та чи інша змінна є масивом? 

Використовуючи метод (аrray.isArray)