let imgesWrapper = document.querySelectorAll(".images-wrapper img");

let i = 0;
let startButton = document.createElement("button");
startButton.innerHTML = "Відновити показ";
// startButton.classList.add("btn");
startButton.addEventListener("click", function () {
  startButton.setAttribute("disabled", "disabled");

  if (startButton) {
    startButton.disabled = true;
    stopShow = setInterval(showImg, 3000);
    stopButton.disabled = false;
    console.log(startButton.disabled);
  } else {
    // startButton.disabled = false;
    // setInterval(showImg, 3000);
  }
});

let stopButton = document.createElement("button");
stopButton.innerHTML = "Припинити";
// stopButton.classList.add("btn");
stopButton.addEventListener("click", function () {
  stopButton.setAttribute("disabled", "disabled");
  if (stopButton) {
    stopButton.disabled = true;
    clearInterval(stopShow);
    startButton.disabled = false;
    console.log(stopButton.disabled);
  } else {
    // stopButton.disabled = false;
    // clearInterval(stopShow);
  }
});

document.body.append(startButton);
document.body.append(stopButton);

function showImg() {
  if (i === imgesWrapper.length - 1) {
    imgesWrapper[i].style.display = "none";
    i = 0;
    imgesWrapper[0].style.display = "block";
  } else {
    imgesWrapper[i].style.display = "none";
    imgesWrapper[i + 1].style.display = "block";
    i++;
  }
}

let stopShow = setInterval(showImg, 3000);
