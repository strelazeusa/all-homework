## Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
 Технологія яка дозволяє браузеру на основі HTML документу створуювати дерово веб стоврінки а JS: змінювати, видалятиб додавати, отримувати певну інформацію від обєктів DOM. Можливість JS зв'язуватися с html.


2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 innerHTML - поветрає текстовий зміст елемента і разом з цим теги HTML документа.
 innerText - повертає текстовий зміст вкладений в дочірній елемент, без тегі html докумена.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Найпопулярніший і частіше використовуваний спосіб звернення це - document.querySelector() і document.querySelectorAll() повертають не живу колекцію.

Другий метод який повертає живу колекцію включає в себе декілька варіантів:
document.getElementById() - повертає обєкт за його айді
document.getElementsByName()  - поветрає елементи за name
document.getElementsByTagName() - повертає елементи за тегом 
document.getElementsByClassName() - повертає елементи за класом 

На мою думку кращий спосіб це document.querySelector() і document.querySelectorAll()

